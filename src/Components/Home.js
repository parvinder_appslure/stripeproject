import React, {useRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {
  CardActionError,
  CardField,
  CardFieldInput,
  CardForm,
  useStripe,
  CardFormView,
} from '@stripe/stripe-react-native';
import {useState} from 'react/cjs/react.development';

const Home = () => {
  const {confirmPayment} = useStripe();
  const [state, setState] = useState({
    last4: '',
    expiryMonth: null,
    expiryYear: null,
    complete: false,
    brand: '',
    country: '',
    postalCode: '',
    saveCard: false,
  });
  const cardRef = useRef(null);
  const onCardScanhandler = () => {
    let ab = cardRef.current;
    debugger;
    console.log('first');
  };
  const onFormComplete = cardDetails => {
    console.log(JSON.stringify(cardDetails, null, 2));
    if (cardDetails.complete) {
      setState({...state, ...cardDetails});
    } else {
      setState({
        ...state,
        last4: '',
        expiryMonth: null,
        expiryYear: null,
        complete: false,
        brand: '',
        country: '',
        postalCode: '',
      });
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.cardWrapper}>
        <TouchableOpacity style={styles.touchClose}>
          <Text style={styles.textClose}>X</Text>
        </TouchableOpacity>
        <Text style={styles.textTitle}>Add Your Pament Information</Text>
        <View style={styles.cardScanWrapper}>
          <Text style={styles.textTitleInfo}>Card Information</Text>
          <TouchableOpacity onPress={onCardScanhandler}>
            <Text style={styles.textTitleInfo}>Scan Card</Text>
          </TouchableOpacity>
        </View>
        <CardForm
          ref={cardRef}
          onFormComplete={onFormComplete}
          CountryEnabled={false}
          postalCodeEnabled={false}
          placeholder={{
            number: '4242 4242 4242 4242',
          }}
          cardStyle={styles.cardStyles}
          style={styles.cardContainerStyle}
          onFocus={focusedField => {
            console.log('focusField', focusedField);
          }}
        />
        <View style={styles.checkBoxWrapper}>
          <CheckBox
            value={state.saveCard}
            onValueChange={saveCard => setState({...state, saveCard})}
            tintColors={{true: 'black', false: 'grey'}}
            boxType={'square'}
            onFillColor={'white'}
            onCheckColor={'black'}
            onTintColor={'grey'}
            lineWidth={1.5}
            style={{
              marginRight: 10,
              width: 20,
              height: 20,
            }}
          />
          <Text style={styles.textSaveCard}>
            Save this card for future Vendors payments
          </Text>
        </View>
        <TouchableOpacity style={styles.buttonTouch}>
          <Text style={styles.buttonText}>Pay $6.42</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: '#0000001a',
  },
  cardStyles: {
    textColor: '#000000',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    marginTop: 40,
    width: '90%',
  },
  cardContainerStyle: {
    backgroundColor: '#ffffff',
    width: '100%',
    alignSelf: 'center',
    height: 170,
  },
  cardWrapper: {
    padding: 20,
    paddingBottom: 40,
    backgroundColor: '#ffffff',
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
  },
  cardScanWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    marginBottom: 10,
  },
  textTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textTitleInfo: {
    fontSize: 14,
    fontWeight: '500',
  },
  buttonTouch: {
    backgroundColor: '#6c8eef',
    borderRadius: 15,
    paddingVertical: 15,
    alignItems: 'center',
  },
  buttonText: {
    color: '#f8f8f8',
  },
  textSaveCard: {
    fontSize: 14,
    color: 'grey',
  },
  checkBoxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  touchClose: {
    marginBottom: 10,
  },
  textClose: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Home;
